from cProfile import label
import numpy as np
import matplotlib.pyplot as plt

# # Execution time
# time_256 = np.array([12, 49, 412, 3378])
# n_reals_256 = np.array([100, 1000, 10000, 100000])
# time_512 = np.array([25, 1462, 6833, 15037])
# n_reals_512 = np.array([100, 10000, 50000, 100000])
# # time_128 = np.array([7, 17, 101, 945, 9619])
# time_128 = np.array([7, 17, 101, 945])
# # n_reals_128 = np.array([100, 1000, 10000, 100000, 1000000])
# n_reals_128 = np.array([100, 1000, 10000, 100000])
# n_reals_128s = np.array([100, 1000, 10000])
# time_128s = np.array([48.697460412979126, 196.868314743042, 1959])

# plt.plot(n_reals_128, time_128)
# plt.plot(n_reals_256, time_256)
# plt.plot(n_reals_512, time_512)
# plt.plot(n_reals_128s, time_128s, color = 'b', linestyle = 'dashed')
# plt.grid()
# plt.title('Execution time of parallel program')
# plt.ylabel('Time (s)')
# plt.xlabel('Channel realizations')
# plt.legend(['N = 128', 'N = 256', 'N = 512'])
# # plt.xscale('log')
# # plt.yscale('log')
# plt.show()

# # Serial vs Parallel plot
# time_64_serial = np.array([18, 28, 42, 56, 70, 83, 138])
# # n_reals_64_serial = np.linspace(100, 600, 6)
# n_reals_64_serial = np.array([100, 200, 300, 400, 500, 600, 1000])

# plt.plot(n_reals_64_serial, time_64_serial)
# # plt.plot(n_reals_64, time_64)
# plt.show()

# # Memory consumption plot
# mem_cons_parallel = np.array([3.17624282836914, 3.20487213134766, 3.20487213134766, 4.95444488525391, 12.01330947875977])
# mem_cons_serial = np.array([0.130859375, 0.134440957, 0.1521766602, 0.206328125, 0.4806963184])
# mem_N = np.array([64, 128, 256, 512, 1024])

# plt.scatter(mem_N, mem_cons_parallel)
# plt.plot(mem_N, mem_cons_parallel, label = 'Parallel')
# plt.scatter(mem_N, mem_cons_serial)
# plt.plot(mem_N, mem_cons_serial, label = 'Serial')
# plt.xlabel('N')
# plt.ylabel('Gigabytes')
# plt.title('Maximum Memory Consumption (n_reals = 100)')
# # plt.legend(['Parallel', 'Serial'])
# plt.legend()
# plt.grid()
# plt.show()

Uout = np.load('output_N_128_nreals_100.npy')
fig2, ax2 = plt.subplots()
  
# plots filled contour plot
N = 128
coos_out = 0.02*np.arange(-N/2, N/2)
x_out, y_out = np.meshgrid(coos_out, coos_out)
c_plot2 = ax2.contourf(x_out, y_out, abs(Uout**2), 100, cmap = plt.cm.inferno)
cbar2 = fig2.colorbar(c_plot2)

ax2.set_title('Output LG beam intensity')
ax2.set_xlabel('X axis')
ax2.set_ylabel('Y axis')

plt.show()
