import aotools as ao
import numpy as np
import math as m
import matplotlib.pyplot as plt

def laguerre_pol(l, p, x):
    if p == 0:
        return 1
    elif p == 1:
        return 1 - x + l
    else:
        return (2*p + l - 1 - x)*laguerre_pol(l, p - 1, x)/p - (p + l - 1)*laguerre_pol(l, p - 2, x)/p

def lag_gaus_beam(wvl, w0, l, p, z, r):
# LAG_GAUS_BEAM
# Function that calculates the radial profile of a Laguerre-Gaussian beam

    k = 2*np.pi/wvl; # Optical wavenumber
    zR = np.pi*(w0**2)/wvl; # Rayleigh range
    w = w0*np.sqrt(1+(z/zR)**2); # Beam-waist radius at distance z

    # Calculate generalized Laguerre polynomial of order zero
    Lpl = laguerre_pol(l, p, 2*r**2/w**2)

    #Calculate the radial profile
    Rpl = 2*np.sqrt(m.factorial(p)/m.factorial(p+abs(l)))*(1/w)*((r*np.sqrt(2)/w)**abs(l))*np.exp(-r**2/w**2)*Lpl*np.exp(1j*k*r**2*z/(2*(z**2+zR**2)))*np.exp(-1j*(2*p+abs(l)+1)*np.arctan2(z, zR))
    
    return Rpl

def oam_eigenstate(wvl, w0, l, p, z, r, theta):
# OAM_EIGENSTATE
# Function that calculates the form of an Orbital Angular Momentum (OAM)
# eigenstate based on the radial profile of a Laguerre-Gaussian (LG) beam

    Rpl = lag_gaus_beam(wvl, w0, l, p, z, r) # Radial profile of LG beam
    phi_pl = np.exp(1j*l*theta)*Rpl/np.sqrt(2*np.pi) # OAM eigenstate

    return phi_pl

def multStepAngularSpectrum(inputComplexAmp, wvl, inputSpacing, outputSpacing, z, screens):
    # If propagation distance is 0, don't bother 
    if np.size(z) == 1:
        return inputComplexAmp

    N = inputComplexAmp.shape[0] # Assumes Uin is square.
    k = 2*np.pi/wvl     # Optical wavevector

    # Grid setup
    n = np.size(z)
    coos = np.arange(-N/2, N/2)
    nx, ny = np.meshgrid(coos, coos)
    nsq = nx**2 + ny**2

    # Propagation distances
    Delta_z = z[1:n] - z[0:n-1]

    # Grid spacings
    alpha = z / z[n-1]
    delta = (1-alpha) * inputSpacing + alpha * outputSpacing
    mag = delta[1:n] / delta[0:n-1]
    x_in = nx * delta[0]
    y_in = ny * delta[0]
    r_insq = x_in**2 + y_in**2

    # Input quadratic phase factor
    Q1 = np.exp( 1j * k/2. * (1-mag[0])/Delta_z[0] * r_insq)

    inputComplexAmp = inputComplexAmp * Q1 * np.exp(1j*screens[0:, 0:, 0])

    for idx in range(0, n-1):

        deltaf = 1 / (N*delta[idx])
        fX = nx * deltaf
        fY = ny * deltaf
        fsq = fX**2 + fY**2
        Z = Delta_z[idx] # Propagation distance

        # Quadratic phase factor
        Q2 = np.exp(-1j*(np.pi**2)*2*Z/mag[idx]/k*fsq)

        # Compute propagated field
        inputComplexAmp = np.exp(1j*screens[0:, 0:, idx+1])  * ao.fouriertransform.ift2(Q2 * ao.fouriertransform.ft2(inputComplexAmp / mag[idx], delta[idx]), deltaf)

    # Observation-plane coordinates
    x_out = nx * delta[n-1]
    y_out = ny * delta[n-1]
    r_outsq = x_out**2 + y_out*2

    # Output quadratic phase factor
    Q3 = np.exp(1j*k/2*(mag[n-2]-1)/(mag[n-2]*Z)*r_outsq)

    # Compute output field
    outputComplexAmp = Q3 * inputComplexAmp

    return outputComplexAmp

wvl = 1550*10**(-9)
delta_in = 0.02
delta_out = 0.02
L = 20*10**(3) # Propagation distance
w0 = 15*10**(-2); # Beamwaist radius
l = 2
p = 0
N = 256

coos_in = delta_in*np.arange(-N/2, N/2)
x_in, y_in = np.meshgrid(coos_in, coos_in)
theta_in = np.arctan2(y_in, x_in)
r_in = np.sqrt(x_in**2 + y_in**2)

Uin = oam_eigenstate(wvl, w0, l, p, 0, r_in, theta_in)
# Uin = np.exp(-(x_in**2 + y_in**2))

fig, ax = plt.subplots(constrained_layout=True)
  
# plots filled contour plot
c_plot = ax.contourf(x_in, y_in, abs(Uin**2), 100, cmap = plt.cm.inferno)
cbar = fig.colorbar(c_plot)

ax.set_title('Input LG beam intensity')
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')

# plt.show()

n_screens = 3
screens = np.zeros((N, N, n_screens))

# Propagation planes positions
z = np.linspace(0, L, n_screens)

# Grid spacings
alpha = z / z[n_screens-1]
delta = (1-alpha) * delta_in + alpha * delta_out

r0 = 0.16
L0 = 100
l0 = 0.01
for idx in range(0, n_screens):
    screens[0:, 0:, idx] = ao.ft_sh_phase_screen(r0, N, delta[idx], L0, l0)
    # screens[0:, 0:, idx] = np.ones([N, N])

Uout = multStepAngularSpectrum(Uin, wvl, delta_in, delta_out, z, screens)
# Uout = ao.opticalpropagation.angularSpectrum(Uin, wvl, delta_in, delta_out, L)
# Uout = oam_eigenstate(wvl, w0, l, p, L, r_in, theta_in)

coos_out = delta_out*np.arange(-N/2, N/2)
x_out, y_out = np.meshgrid(coos_out, coos_out)

# zR = np.pi*(w0**2)/wvl
# R = L*(1+(zR/L)**2)
# Uout = Uout * np.exp(-1j*np.pi/(wvl*R)*(x_out**2+y_out**2))

fig2, ax2 = plt.subplots(constrained_layout=True)
  
# plots filled contour plot
c_plot2 = ax2.contourf(x_out, y_out, abs(Uout**2), 100, cmap = plt.cm.inferno)
cbar2 = fig2.colorbar(c_plot2)

ax2.set_title('LG beam intensity')
ax2.set_xlabel('X axis')
ax2.set_ylabel('Y axis')

plt.show()

