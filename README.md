## Parallel Numerical Optical Propagation
Repository for the 2021-2022 PhD course *"Advanced topics on scientific and parallel programming with practical application on the CAPRI HPC infrastructure"* at the University of Padova. Contains Python script for paralellizing numerical propagation of optical waves using `multiprocessing` and using parts of the [AOtools](https://github.com/AOtools) library. 

### Structure
```
parallel_nop
├─ .gitignore                
├─ cont_def.txt            # Singularity container definition file
├─ cont_img.sif            # Singularity container image
├─ parallel_job.slurm      # Slurm file for parallel script scheduling
├─ README.md               
├─ serial_job.slurm        # Slurm file for serial script scheduling
└─ source
   ├─ hpc_parallel.py      # Parallel python script
   ├─ hpc_serial.py        # Serial python script
   ├─ main.py              # Script used for development
   └─ propagation.ipynb    # Jupyter notebook used for debugging

```

### Functionality

* At the moment, the script parallelizes only the realization of independent instances of atmospheric turbulence, that can be used for the further extraction of statistical properties as the user sees fit. 

* Functions for the generation of Laguerre-Gaussian optical beams are included.